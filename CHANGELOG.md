# 1.0.0 (2024-11-17)


### Bug Fixes

* spec erorr build job ([8985f4d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/8985f4dbf0a426d9011672550b757dfb7b303245))
* testing patch releases ([6a4c7ab](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/6a4c7ab9aea9ed668b0a1a2ba83049e3c656fbce))
* testing patch releases ([418da7b](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/418da7b579069ff196b0c251b9160f685e201fca))
* testing patch releases ([c34ec04](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c34ec04d061695b76fcbee27d56518d4dd7190cd))
* testing patch releases ([c1be6e4](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c1be6e48fa56047ac56e1e9910899c64b403177e))
* testing patch releases ([600d116](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/600d116501a83d0f8dba1947b5feccc49988f12d))
* testing patch releases ([4b5dce7](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4b5dce75bbde806bd5027e12cf10714b74fa0d19))
* testing patch releases ([737860d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/737860d5adc86a391034c51954257757aae6a174))
* testing patch releases ([c310419](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c310419b8ea716f2015ac0a3734c627f96b5d372))
* testing patch releases ([eb14042](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/eb140425a9ad31442272b07a5ea10595372ea490))
* testing patch releases ([20829ac](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/20829ac3f629d67849b96b571b71bd5e342137d1))
* testing patch releases ([b34e086](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/b34e086046fcf8718c8eeed687488fa0f978a745))
* testing patch releases ([07ab852](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/07ab852f8b5579ea0965a98cc1c1ad9a900ff7fd))
* testing patch releases ([9435b90](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/9435b900b40eaf9055b92df51601ed23ed650035))
* xxx ([baff48a](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/baff48a5a35d31ecd8739eb5d090d60c8988f30e))
* xxx ([79a7cba](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/79a7cbabec6145cf784a231e78e013174bb55091))
* xxx ([8f11f7e](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/8f11f7ea6913b1ff0a5b1449fefc3f09344c5584))
* xxx ([d6d6591](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/d6d65911ca577b95ff9a151c0aecee6aa6688faf))
* xxx ([5c372ca](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/5c372ca25e92a248601c525717b013da8cb5f9d1))
* xxx ([9929fb9](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/9929fb92d5c6b476eaca9a53607cd6284909e321))
* xxx ([e25e393](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/e25e393d215db3f2ea16e87d70773a1e74c92ee7))
* xxx ([4303706](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4303706eb33e5a49ad743d1918985f1122cd5873))
* xxx ([060bb61](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/060bb6137f3173bceedd693072954121f531e29b))
* xxx ([befc8ab](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/befc8abea2dcaafc85d001b2cad17b2d049d2ec6))
* xxx ([ef2ab9e](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/ef2ab9ed2c3aec3308b03401ed06865c405d0928))
* xxxx ([8e319de](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/8e319deee9f3c6c478cedb8825610565aa611133))


### Features

* add auth for OR ([d55d5b6](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/d55d5b652291cac1fdf17b316a43d6f9a6f690b0))
* add auth for OR ([70fa6ae](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/70fa6ae42a64d6df15161e2e9ce0e6c805e525d6))
* add auth for OR ([caeef20](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/caeef200e27d811b5c3ac847d0ab37e33fb41e66))
* add auth for test ([652567c](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/652567c015e44e5561d92153e815d2e83f4b4ed9))
* add auth for test ([36fa058](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/36fa05807a1785b67f2fee319debd8aa1da1671c))
* add auth for test ([166b553](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/166b553c60f1413e812bdea79aa927f24e987458))
* add auth for test ([9143aea](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/9143aea222ed81b6e4dae33d4bb14bfd6937ee03))
* add auth for test ([c0f117f](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c0f117f2bfeb18ef8bde63839f2bc9daaa62ecda))
* add auth for test ([a4855ce](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/a4855ce3e33d7808eb6041855f270d3fb241ff9d))
* add auth for test ([c266243](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c2662431d52da0bb8dda4173712896ad8a8b545d))
* add auth for test ([8110283](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/8110283c84cf7151b2d5369c400ae90a236b5ff7))
* add auth for test2 ([eb6feae](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/eb6feaef9d1d96ec34fbb490b762a712316bab57))
* add auth for test3 ([03a9df9](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/03a9df94745167928e1f0736b8341eee49734aca))
* add auth for test3 ([4df13e3](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4df13e3526c3112713730bd1110165323b677178))
* add auth for test3 ([20858bd](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/20858bd56885f25f26ffba8dca1ea6836cbe4b23))
* add auth for test3 ([3e03d02](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/3e03d02511c223afadc39e58f0d7e13af4b0a37f))
* add auth for test4 ([698c38c](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/698c38c76098cc774771cb55f1448d7aca21edb7))
* add auth for test5 ([2ac79e7](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/2ac79e789295450ed036105f961aee6fb10b48bf))
* add auth for test6 ([27517d8](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/27517d8011d59db3e7a7c482088b2ca08ec60b57))
* add auth for test6 ([c61c043](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c61c043d67e81539c2f9e463e5a358e8b3a92026))
* add auth for testx ([e6216f4](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/e6216f4bbf5ad8d3391c1f40afb3d0dd77684c3a))
* add auth for testxx ([4a9f3d5](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4a9f3d5f22f7c26185eba04ec81344233fc03e9d))
* add auth for testxx ([824fba7](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/824fba7db6455ee51aa3c8e4c2570c5f0ab944d2))
* add auth for testxxx ([b918b53](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/b918b5359d511beaf0380521b0f52a3fa1a34941))
* add auth for testxxx ([5005540](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/5005540963b7e91828acd863956c73b82404a125))
* add auth for testxxx ([dfddb51](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/dfddb510bbffae339854d7ac5ebc74ec2481f176))
* add auth for testxxx ([4a08480](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4a084808a960ff88c3c968d8f7c96516f838eeb7))
* add auth for testxxx ([5961c56](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/5961c563dd5bd2486a09d5556931b8c8d1dd270c))
* add auth for testxxx ([798bb1d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/798bb1d673b9ca4f5e6a57d963d0954175d740a5))
* add auth for testxxxx ([f1541e3](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/f1541e37a60c27449cbfc76dbb7203c64128ac73))
* add auth for testxxxx ([c672f9d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c672f9de35cdfcad13717bb8fd10f08e6f7f43b5))
* add auth for testxxxx ([f3b03c9](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/f3b03c91ed75f0aef1051e30721a07a9a7ddd0aa))
* ci ([669a539](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/669a539574ffeee9c43fb80a388d607593de244e))
* feat: testing minor releases ([dacb60e](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/dacb60e345118ac28aa84bda79f438ee89b5bc04))
* minor ([ec4effe](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/ec4effea0cd463746886be1c1f93dfc9ea372af6))
* retest ([64c1314](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/64c131400c72a18ec5ecee507108a2b13642a85f))
* retest ([c6773d2](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c6773d27f23822b8973a4c8272793bf12376d4dd))
* retest ([260dbed](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/260dbedc3eb4d570e23a8c7b8621226f2d658270))
* test ([05933bc](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/05933bc1f1b4f0da6c975ae78972e89ccf010d17))
* test ([7a3d896](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/7a3d89652f1314b6278430832dffbb1d3b62b0df))
* test ([2f9cc73](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/2f9cc73b524ea628d7f92565106a522341f392c6))
* test ([3a7042d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/3a7042d2b6990cdc80e20d09c7c8ee6ce519c766))
* test ([2ff7747](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/2ff7747dc672e9228e01fea1e2265de81480fe07))
* xx ([a20852d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/a20852d17f6eebd7c961f93da282b56cbd84af64))
* xxx ([eac3b57](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/eac3b572b184e44914836cbeb61ff3f47d113462))
* xxx ([b39c64a](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/b39c64a6927905d487315973cada28f7a875751c))
* xxx ([231d596](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/231d59602f378355581c8c7537dc3b88036af83c))
* xxx ([ce855ef](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/ce855efc24e93566ef89d673a86c3b878bd9cc6f))
* xxx ([b1bdbb2](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/b1bdbb2cb3793cdb7341bc95d7a715181860d7be))


### Performance Improvements

* **pencil:** remove graphiteWidth option ([f0522b9](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/f0522b98fcfb28ed5d1824dd43899ea0a4068163))


### BREAKING CHANGES

* **pencil:** The graphiteWidth option has been removed.
The default graphite width of 10mm is always used for performance reasons.

# [1.6.0](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/compare/admin-1.5.0...admin-1.6.0) (2024-11-17)


### Features

* add auth for test4 ([698c38c](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/698c38c76098cc774771cb55f1448d7aca21edb7))
* add auth for test5 ([2ac79e7](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/2ac79e789295450ed036105f961aee6fb10b48bf))
* add auth for test6 ([27517d8](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/27517d8011d59db3e7a7c482088b2ca08ec60b57))
* add auth for test6 ([c61c043](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c61c043d67e81539c2f9e463e5a358e8b3a92026))
* retest ([260dbed](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/260dbedc3eb4d570e23a8c7b8621226f2d658270))
* test ([05933bc](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/05933bc1f1b4f0da6c975ae78972e89ccf010d17))
* test ([7a3d896](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/7a3d89652f1314b6278430832dffbb1d3b62b0df))
* test ([2f9cc73](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/2f9cc73b524ea628d7f92565106a522341f392c6))
* test ([3a7042d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/3a7042d2b6990cdc80e20d09c7c8ee6ce519c766))
* test ([2ff7747](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/2ff7747dc672e9228e01fea1e2265de81480fe07))

# [1.6.0](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/compare/test-1.5.0...test-1.6.0) (2024-06-09)


### Features

* test ([05933bc](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/05933bc1f1b4f0da6c975ae78972e89ccf010d17))

# [1.5.0](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/compare/test-1.4.0...test-1.5.0) (2024-06-09)


### Features

* test ([7a3d896](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/7a3d89652f1314b6278430832dffbb1d3b62b0df))

# [1.4.0](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/compare/test-1.3.0...test-1.4.0) (2024-06-09)


### Features

* test ([2f9cc73](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/2f9cc73b524ea628d7f92565106a522341f392c6))

# [1.2.0](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/compare/test-1.1.0...test-1.2.0) (2024-06-09)


### Features

* test ([2ff7747](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/2ff7747dc672e9228e01fea1e2265de81480fe07))

# [1.1.0](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/compare/test-1.0.0...test-1.1.0) (2024-06-04)


### Features

* add auth for test6 ([27517d8](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/27517d8011d59db3e7a7c482088b2ca08ec60b57))

# 1.0.0 (2024-06-03)


### Bug Fixes

* spec erorr build job ([8985f4d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/8985f4dbf0a426d9011672550b757dfb7b303245))
* testing patch releases ([6a4c7ab](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/6a4c7ab9aea9ed668b0a1a2ba83049e3c656fbce))
* testing patch releases ([418da7b](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/418da7b579069ff196b0c251b9160f685e201fca))
* testing patch releases ([c34ec04](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c34ec04d061695b76fcbee27d56518d4dd7190cd))
* testing patch releases ([c1be6e4](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c1be6e48fa56047ac56e1e9910899c64b403177e))
* testing patch releases ([600d116](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/600d116501a83d0f8dba1947b5feccc49988f12d))
* testing patch releases ([4b5dce7](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4b5dce75bbde806bd5027e12cf10714b74fa0d19))
* testing patch releases ([737860d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/737860d5adc86a391034c51954257757aae6a174))
* testing patch releases ([c310419](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c310419b8ea716f2015ac0a3734c627f96b5d372))
* testing patch releases ([eb14042](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/eb140425a9ad31442272b07a5ea10595372ea490))
* testing patch releases ([20829ac](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/20829ac3f629d67849b96b571b71bd5e342137d1))
* testing patch releases ([b34e086](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/b34e086046fcf8718c8eeed687488fa0f978a745))
* testing patch releases ([07ab852](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/07ab852f8b5579ea0965a98cc1c1ad9a900ff7fd))
* testing patch releases ([9435b90](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/9435b900b40eaf9055b92df51601ed23ed650035))
* xxx ([baff48a](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/baff48a5a35d31ecd8739eb5d090d60c8988f30e))
* xxx ([79a7cba](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/79a7cbabec6145cf784a231e78e013174bb55091))
* xxx ([8f11f7e](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/8f11f7ea6913b1ff0a5b1449fefc3f09344c5584))
* xxx ([d6d6591](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/d6d65911ca577b95ff9a151c0aecee6aa6688faf))
* xxx ([5c372ca](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/5c372ca25e92a248601c525717b013da8cb5f9d1))
* xxx ([9929fb9](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/9929fb92d5c6b476eaca9a53607cd6284909e321))
* xxx ([e25e393](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/e25e393d215db3f2ea16e87d70773a1e74c92ee7))
* xxx ([4303706](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4303706eb33e5a49ad743d1918985f1122cd5873))
* xxx ([060bb61](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/060bb6137f3173bceedd693072954121f531e29b))
* xxx ([befc8ab](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/befc8abea2dcaafc85d001b2cad17b2d049d2ec6))
* xxx ([ef2ab9e](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/ef2ab9ed2c3aec3308b03401ed06865c405d0928))
* xxxx ([8e319de](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/8e319deee9f3c6c478cedb8825610565aa611133))


### Features

* add auth for OR ([d55d5b6](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/d55d5b652291cac1fdf17b316a43d6f9a6f690b0))
* add auth for OR ([70fa6ae](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/70fa6ae42a64d6df15161e2e9ce0e6c805e525d6))
* add auth for OR ([caeef20](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/caeef200e27d811b5c3ac847d0ab37e33fb41e66))
* add auth for test ([652567c](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/652567c015e44e5561d92153e815d2e83f4b4ed9))
* add auth for test ([36fa058](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/36fa05807a1785b67f2fee319debd8aa1da1671c))
* add auth for test ([166b553](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/166b553c60f1413e812bdea79aa927f24e987458))
* add auth for test ([9143aea](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/9143aea222ed81b6e4dae33d4bb14bfd6937ee03))
* add auth for test ([c0f117f](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c0f117f2bfeb18ef8bde63839f2bc9daaa62ecda))
* add auth for test ([a4855ce](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/a4855ce3e33d7808eb6041855f270d3fb241ff9d))
* add auth for test ([c266243](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c2662431d52da0bb8dda4173712896ad8a8b545d))
* add auth for test ([8110283](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/8110283c84cf7151b2d5369c400ae90a236b5ff7))
* add auth for test2 ([eb6feae](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/eb6feaef9d1d96ec34fbb490b762a712316bab57))
* add auth for test3 ([03a9df9](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/03a9df94745167928e1f0736b8341eee49734aca))
* add auth for test3 ([4df13e3](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4df13e3526c3112713730bd1110165323b677178))
* add auth for test3 ([20858bd](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/20858bd56885f25f26ffba8dca1ea6836cbe4b23))
* add auth for test3 ([3e03d02](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/3e03d02511c223afadc39e58f0d7e13af4b0a37f))
* add auth for test4 ([698c38c](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/698c38c76098cc774771cb55f1448d7aca21edb7))
* add auth for test5 ([2ac79e7](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/2ac79e789295450ed036105f961aee6fb10b48bf))
* add auth for test6 ([c61c043](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c61c043d67e81539c2f9e463e5a358e8b3a92026))
* add auth for testx ([e6216f4](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/e6216f4bbf5ad8d3391c1f40afb3d0dd77684c3a))
* add auth for testxx ([4a9f3d5](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4a9f3d5f22f7c26185eba04ec81344233fc03e9d))
* add auth for testxx ([824fba7](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/824fba7db6455ee51aa3c8e4c2570c5f0ab944d2))
* add auth for testxxx ([b918b53](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/b918b5359d511beaf0380521b0f52a3fa1a34941))
* add auth for testxxx ([5005540](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/5005540963b7e91828acd863956c73b82404a125))
* add auth for testxxx ([dfddb51](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/dfddb510bbffae339854d7ac5ebc74ec2481f176))
* add auth for testxxx ([4a08480](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4a084808a960ff88c3c968d8f7c96516f838eeb7))
* add auth for testxxx ([5961c56](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/5961c563dd5bd2486a09d5556931b8c8d1dd270c))
* add auth for testxxx ([798bb1d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/798bb1d673b9ca4f5e6a57d963d0954175d740a5))
* add auth for testxxxx ([f1541e3](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/f1541e37a60c27449cbfc76dbb7203c64128ac73))
* add auth for testxxxx ([c672f9d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/c672f9de35cdfcad13717bb8fd10f08e6f7f43b5))
* add auth for testxxxx ([f3b03c9](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/f3b03c91ed75f0aef1051e30721a07a9a7ddd0aa))
* ci ([669a539](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/669a539574ffeee9c43fb80a388d607593de244e))
* feat: testing minor releases ([dacb60e](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/dacb60e345118ac28aa84bda79f438ee89b5bc04))
* minor ([ec4effe](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/ec4effea0cd463746886be1c1f93dfc9ea372af6))
* xx ([a20852d](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/a20852d17f6eebd7c961f93da282b56cbd84af64))
* xxx ([eac3b57](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/eac3b572b184e44914836cbeb61ff3f47d113462))
* xxx ([b39c64a](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/b39c64a6927905d487315973cada28f7a875751c))
* xxx ([231d596](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/231d59602f378355581c8c7537dc3b88036af83c))
* xxx ([ce855ef](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/ce855efc24e93566ef89d673a86c3b878bd9cc6f))
* xxx ([b1bdbb2](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/b1bdbb2cb3793cdb7341bc95d7a715181860d7be))


### Performance Improvements

* **pencil:** remove graphiteWidth option ([f0522b9](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/f0522b98fcfb28ed5d1824dd43899ea0a4068163))


### BREAKING CHANGES

* **pencil:** The graphiteWidth option has been removed.
The default graphite width of 10mm is always used for performance reasons.

# [1.5.0](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/compare/admin-1.4.0...admin-1.5.0) (2024-06-02)


### Features

* add auth for test3 ([03a9df9](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/03a9df94745167928e1f0736b8341eee49734aca))

# [1.4.0](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/compare/admin-1.3.0...admin-1.4.0) (2024-06-02)


### Features

* add auth for test3 ([4df13e3](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/4df13e3526c3112713730bd1110165323b677178))
* add auth for test3 ([20858bd](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/20858bd56885f25f26ffba8dca1ea6836cbe4b23))

# [1.3.0](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/compare/admin-1.2.0...admin-1.3.0) (2024-06-02)


### Features

* add auth for test2 ([eb6feae](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/eb6feaef9d1d96ec34fbb490b762a712316bab57))

# [1.2.0](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/compare/admin-1.1.0...admin-1.2.0) (2024-06-02)


### Features

* add auth for testxxxx ([f1541e3](https://gitlab.com/chinwanbbgz/gitlab-semantic-versioning/commit/f1541e37a60c27449cbfc76dbb7203c64128ac73))
